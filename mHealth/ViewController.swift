//
//  ViewController.swift
//  mHealth
//
//  Created by Yongsung on 1/16/16.
//  Copyright © 2016 Delta. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {
    @IBOutlet weak var lightTextField: UITextField!

    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var stepLabel: UILabel!
    
    let pedometer = CMPedometer()
    var steps = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lightTextField.text = "0"
        // Do any additional setup after loading the view, typically from a nib.
        let fromDate = NSDate(timeIntervalSinceNow: -86400)
        if(CMPedometer.isStepCountingAvailable()){
            pedometer.startPedometerUpdatesFromDate(NSDate()){
                data, error in
                if error != nil {
                    print("There was an error requesting data from the pedometer: \(error)")
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        print("Steps Taken: \(data!.numberOfSteps)")
                        self.stepLabel.text = "\(data!.numberOfSteps.integerValue + self.steps)"
                        self.data_request()
                    }
                }
            }
            
            pedometer.queryPedometerDataFromDate(fromDate, toDate: NSDate()){
                (data, error) -> Void in
                if error != nil {
                    print("There was an error requesting data from the pedometer: \(error)")
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.steps = data!.numberOfSteps.integerValue
                        self.stepLabel.text = "\(data!.numberOfSteps)"
                        self.data_request()
                    }
                    //                    print("Steps Taken: \(data!.numberOfSteps)")
                }
            }
            

            

        } else {
            print("step count not availble")
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClick(sender: UIButton) {
        let threshold: Int = Int(self.lightTextField.text!)!
        let step: Int = Int(self.stepLabel.text!)!
        data_request()
        if step < threshold {
            print("Hello, you should walk more: \(step)")
        } else {
            print("World, you are in good shape: \(step)")
        }
    }
    
    var url_to_request = "https://limitless-retreat-47863.herokuapp.com/api/testapp/"
    
    func data_request()
    {
        let url:NSURL = NSURL(string: url_to_request)!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let threshold:Float = Float(self.lightTextField.text!)!
        let step:Float = Float(self.stepLabel.text!)!
        let paramString = "name=Pig&steps=\(step)&threshold=\(threshold)"
        request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = session.dataTaskWithRequest(request) {
            (
            let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("There was an error psosting data: \(error)")
                return
            }
            
            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print(dataString)
            
        }
        
        task.resume()
        
    }
}

