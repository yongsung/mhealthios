#!/bin/bash
# original source from http://www.thecave.com/2014/09/16/using-xcodebuild-to-export-a-ipa-from-an-archive/

rm mHealth.ipa
xcodebuild clean -project mHealth -configuration Release -alltargets
xcodebuild archive -project mHealth.xcodeproj -scheme mHealth -archivePath mHealth.xcarchive
xcodebuild -exportArchive -archivePath mHealth.xcarchive -exportPath mHealth -exportFormat ipa -exportProvisioningProfile "Delta"

